package com.jean.mains;

import static org.junit.Assert.*;

import org.junit.Test;

public class MyTest {

	@Test
	public void testSuma() {
		
		int resultado = Calculos.suma(1, 3);
		int esperado = 4;
		
		assertEquals(esperado, resultado );
		
	}
	
	@Test
	
	public void testEdad(){
		
		String resultado= Calculos.edad(17);
		
		String esperado="Es mayor";

		if(resultado.equals(esperado)){
			
			assertEquals(esperado, resultado);
		}else{
			
			fail("No es mayor de edad");
		}
		
	}

}
